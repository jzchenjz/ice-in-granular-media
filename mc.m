classdef mc
    % Run Monte Carlo calculations to find the film thickness, principal radii,
    % and maximum pore/throat radius in packed spherical particles, and
    % post-process the simulation results.

    methods (Static)

        % ----- Monte Carlo calculation ----- %

        function simulate(P, sedfile, name, z, k)
            % Calculate the film thickness, and principal radii of ice
            % near P test points within cut plane z.

            particles = getfield(load(sedfile), 'particles');

            if nargin < 5

                if nargin < 4
                    z = max(particles(:, 3)) / 2; % default cut plane
                end

                k = 40; % choose an appropriate value
            end

            particles = getfield(load(sedfile), 'particles');
            r = particles(:, 4);
            meanR = mean(r);
            maxR = max(r);

            testR = k * meanR;
            meanxy = mean(particles(:, 1:2));

            % in mono-dispersed case, one maxR gives wrong crevice
            highid = particles(:, 3) < z + 2 * maxR; %
            lowid = particles(:, 3) > z - 2 * maxR;
            particles = particles(highid & lowid, :);

            tf = zeros(P, 1); % film thickness
            rf = zeros(P, 1); % film radius
            rp1 = zeros(P, 1); % first principal radius, positive
            rp2 = zeros(P, 1); % second principal radius, negative
            rp = zeros(P, 1); % pore radius
            rt = zeros(P, 1); % throat radius

            disp(['Number of test points: ', num2str(P)])

            rtest = testR * sqrt(rand(P, 1));
            theta = 2 * pi * rand(P, 1);
            ppos = meanxy + rtest .* [cos(theta), sin(theta)];
            ppos(:, 3) = z;

            % view the test points
            % sediment.xsection(sedfile, zcut);
            % hold on
            % scatter(ppos(:, 1), ppos(:, 2), 'r*')

            tstart = tic;

            for i = 1:P
                nP = P / 100;

                if mod(i, nP) == 0
                    disp(['Tested points: ', num2str(i)]);
                end

                ptest = ppos(i, :);

                [ids, dists] = grow.near(ptest, particles);
                pnear = particles(ids, :);

                if ~grow.cut(ptest, pnear) % not within a particle
                    % film
                    rf(i) = pnear(1, 4); % radius of the nearest particle
                    tf(i) = dists(1);
                    % crevice
                    if grow.cone(ptest, pnear(1, :), pnear(2, :))
                        [ro, ri, ~] = grow.toroidal(ptest, pnear(1:2, :));
                        rp1(i) = ro;
                        rp2(i) = ri;
                    end

                    % pore and throat
                    [sp, ispore] = grow.inscribe(ptest, pnear);

                    if ispore
                        rp(i) = sp(4);
                    else
                        rt(i) = sp(4);
                    end

                end

            end

            % save results
            timestamp = datestr(now, 'mm_dd_yyyy_HH_MM');
            telapsed = toc(tstart);
            fprintf('Elapsed time: %f seconds\n', telapsed);

            if nargin < 3
                name = 'sc';
            end

            filename = ['mc_', name, '_', timestamp, '.txt'];

            % header info: sedment model name, number of test points, timestamp
            fid = fopen(filename, 'wt');
            fprintf(fid, 'Timestamp: %s\n', datestr(datenum(timestamp, ...
            'mm_dd_yyyy_HH_MM')));
            fprintf(fid, 'Run time: %f seconds\n', telapsed);
            fprintf(fid, 'Sediment model: %s\n', sedfile);
            fprintf(fid, 'Test points: %d\n', P);
            fprintf(fid, 'pos(x, y, z), tf, rf, rp1, rp2, rp, rt\n');
            fprintf(fid, '---------------\n');
            fclose(fid);
            dlmwrite(filename, [ppos, tf, rf, rp1, rp2, rp, rt], ...
            '-append');
        end

        % ----- Post-processing ----- %

        function [Tf, Slf, Tc, Slc, Tp, Slp, Ttotal, Sltotal] = compile(mcdata)
            % Read the results calculated by Monte Carlo methods and
            % calculate the undercooling of each type (film, crevice, pore +
            % throat, and the combined effect.

            file = importdata(mcdata, ',', 6);
            data = file.data;
            df = data(:, 4);
            rf = data(:, 5);
            rp1 = data(:, 6);
            rp2 = data(:, 7);
            rp = data(:, 8);
            rt = data(:, 9);

            idx_film = df > 0;
            idx_pore = rp > 0;
            idx_throat = rt > 0;
            idx_crevice = rp1 > 0;
            P = size(data, 1);
            numliquid = sum(idx_film | idx_pore | idx_throat | idx_crevice);
            disp(['Estimated porosity: ', num2str(numliquid / P)])

            Tfilm = zeros(P, 1);
            Tpore = zeros(P, 1); % temperature threshold for pore
            Tthroat = zeros(P, 1); % temperature threshold for throat
            Tcrevice = zeros(P, 1); % temperature threshold for crevice

            Tfilm(idx_film) = undercooling.film(rf(idx_film), df(idx_film));
            Tpore(idx_pore) = undercooling.pore(rp(idx_pore));
            Tthroat(idx_throat) = undercooling.pore(rt(idx_throat));
            Tcrevice(idx_crevice) = undercooling.crevice(rp1(idx_crevice), ...
                rp2(idx_crevice));

            % film
            Tf = sort(Tfilm(Tfilm > 0), 'descend');
            Slf = (1:length(Tf))' / numliquid;

            % crevice
            Tc = sort(Tcrevice(Tcrevice > 0), 'descend');
            Slc = (1:length(Tc))' / numliquid;

            % pore and throat
            Tpt = Tpore + Tthroat;
            Tp = sort(Tpt(Tpt > 0), 'descend');
            Slp = (1:length(Tp))' / numliquid;

            % combined effect
            T = zeros(P, 3);
            T(:, 1) = Tfilm;
            T(:, 2) = Tcrevice;
            T(:, 3) = Tpt;
            Tliq = max(T, [], 2); % three categories
            indliquid = Tliq > 0;
            Ttotal = sort(Tliq(indliquid), 'descend');
            Sltotal = (1:length(Ttotal))' / numliquid;
        end

        function plot(mcdata, varargin)
            % Read the results calculated by Monte Carlo methods and plot
            % the data. If the initial weight percent of solute is provided, the
            % colligative effect is accounted.

            if ~isempty(varargin)
                solute = varargin{1};
                mb = varargin{2}; % 30 mM
            else
                solute = 'MgCl2';
                mb = 0;
            end

            [Tf, Slf, Tc, Slc, Tp, Slp, Ttotal, Sltotal] = mc.compile(mcdata);

            % plot
            close all
            figure

            % film curve

            plot(Tf, Slf, 'r*', 'MarkerSize', 4)
            set(gca, 'XScale', 'log')
            set(gca, 'YScale', 'log')
            hold on

            % crevice curve

            plot(Tc, Slc, 'b+', 'markersize', 4)

            % pore + throat curve

            plot(Tp, Slp, 'cs', 'markersize', 4)

            % total curve

            undersolute = undercooling.freezing(mb ./ Sltotal, solute);
            plot(Ttotal - undersolute, Sltotal, 'k', 'LineWidth', 2)

            xlabel('$T_m-T$ (K)', 'interpreter', 'latex', 'FontSize', 18)
            ylabel('$S_l$', 'interpreter', 'latex', 'FontSize', 18)
            xlim([1e-4, 10])
            ylim([1e-4, 1])
        end

        function fit(mcdata, slmin, slmax)
            % Read the results calculated by Monte Carlo methods
            % and plot the data alongside a logarithmic fit between slmin
            % and slmax

            if nargin < 2
                slmin = 1e-4;
                slmax = 0.5;
            end

            % total curve
            [~, ~, ~, ~, ~, ~, Ttotal, Sltotal] = mc.compile(mcdata);

            Tsmall = Sltotal < slmax;
            Slfit = Sltotal(Tsmall);
            Tsfit = Ttotal(Tsmall);
            Tbig = Slfit > slmin;
            Slfit = Slfit(Tbig);
            Tsfit = Tsfit(Tbig)';
            p = polyfit(log(Tsfit), log(Slfit), 1);
            disp(['best fit exponent ', num2str(p(1))])
            disp(['best fit Delta Tf ', num2str((exp(- p(2))).^(1 / p(1)))])
            Srange = [slmin slmax];
            Tfit = (Srange * exp(- p(2))).^(1 / p(1));
            loglog(Ttotal, Sltotal, 'b', 'LineWidth', 2)
            hold on
            loglog(Tfit, Srange, 'r--', 'LineWidth', 2);

            xlabel('$T_m-T$ (K)', 'interpreter', 'latex', 'FontSize', 18)
            ylabel('$S_l$', 'interpreter', 'latex', 'FontSize', 18, 'rotation', 0)
            l = legend('total', 'fit');
            set(l, 'FontSize', 16)
        end

        % ----- Auxiliary calculations ----- %

        function V = crevice_poloidal(r1)
            % CREVICE_POLOIDAL Calculate the volume of the revolving meniscus
            % using the first principal radius, which is the radius of the
            % poloidal circle.

            r = 1;
            alpha = asin(r ./ (r + r1));
            V = 2 * pi * r1.^2 .* (r - sqrt(r1 .* (r1 + 2 * r)) .* alpha);
        end

        function [r1, V] = crevice_toroidal(r2)
            % CREVICE_TOROIDAL Calculate the first principal radius and the
            % volume of the revolving meniscus given the (positive) r2, which is
            % the radius of the toroidal circle.

            r = 1; % radius of contacting particles
            r1 = r2.^2/2 ./ (r - r2);
            V = mc.crevice_poloidal(r1);
        end

    end

end
