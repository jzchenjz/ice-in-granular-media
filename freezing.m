classdef freezing
    % Freezing in soils simulated using the Monte Carlo methods, mostly
    % post-processing.

    methods (Static)

        % ----- Idealized packing ----- %

        function sc
            % Plot the Monte Carlo simulation for simple cubic pack of
            % particles with radius 1 μm, and compare the result with
            % theoretical value.

            % sedfile = '../../Sediment/models/deposit/sc-67626.mat';
            % mc.simulate(P, sedfile, 'sc', 24, 26)

            mcdata = 'data/mc/mc_sc_05_22_2019_15_14.txt';

            % crevice and pore + throat
            [~, ~, Tc, Slc, Tp, Slp, ~, ~] = mc.compile(mcdata);

            figure
            r = 1; % 1 μm
            dT = linspace(0.01, 0.5);
            % Cahn et al. (1992) eq. 8a
            f = 1e-3 * (6.61 * dT.^(- 1/3) / r + 2.26 * (r * dT).^(- 2));
            plot(dT, f, 'k', 'LineWidth', 2)
            hold on

            line_fewer_markers(Tp, Slp, 20, ...
                'b+-', 'spacing', 'curve', 'LineWidth', 2, 'MarkerSize', 4);

            line_fewer_markers(Tc, Slc, 20, ...
                'gs-', 'spacing', 'curve', 'LineWidth', 2, 'MarkerSize', 4);

            xlabel('undercooling (K)', 'FontSize', 18)
            ylabel('$S_l$', 'FontSize', 20, 'interpreter', 'latex')
            xlim([0, 0.5])
            ylim([0, 1])

            % maximum pore r = sqrt(3)-1
            V0 = 2^3 - 4/3 * pi;
            r1 = sqrt(3) - 1;
            V1 = 4/3 * pi * r1^3;
            Sl1 = 1 - V1 / V0;
            T1 = undercooling.pore(r1);
            plot([0, T1, T1], [Sl1, Sl1, 0], 'm--', 'LineWidth', 2, ...
                'HandleVisibility', 'off')

            % volume of the union of 12 quater tori by calculating the
            % corresponding crevice volume
            r2 = 2 - sqrt(2);
            [r1, V2] = mc.crevice_toroidal(r2);
            Sl2 = 3 * V2 / V0;

            T2 = undercooling.pore(r1);
            plot([0, T2, T2], [Sl2, Sl2, 0], 'c--', 'LineWidth', 2, ...
                'HandleVisibility', 'off')

            % crevice
            r2 = linspace(0.1, 2/3);
            [r1, V3] = mc.crevice_toroidal(r2);
            Sl3 = 3 * V3 / V0;
            T3 = undercooling.crevice(r1, - r2);
            plot(T3, Sl3, 'r--', 'LineWidth', 2)

            % throat
            r2 = linspace(0.1, 2 - sqrt(2), 50);
            [r1, V4] = mc.crevice_toroidal(r2);
            Sl4 = 3 * V4 / V0;
            T4 = undercooling.pore(r1);
            plot(T4, Sl4, 'k--', 'LineWidth', 2)

            legend('Cahn et al. (1992)', 'throat total', 'crevice total', ...
                'theoretical crevice', 'theorectial throat')
        end

        % ----- Mono-dispersed packing ----- %

        function cahn(R)
            % Plot the Monte Carlo simulation of experiments in Cahn et al.
            % (1992). Note the difference between the fit parameters and the
            % real particle size used in simulation.

            if R == 0.12
                data = csvread('data/soil/cahn_graphitized.csv');
                dT = logspace(- 1, 2);
                mcdata = 'data/mc/mc_cahn_graphitized_05_22_2019_15_54.txt';
                lscale = 0.5;
            elseif R == 2.5
                data = csvread('data/soil/cahn_polystyrene.csv');
                dT = logspace(- 2, 2);
                mcdata = 'data/mc/mc_cahn_polystyrene_08_12_2019_12_42.txt';
                lscale = 0.5;
            end

            figure
            errorbar(data(:, 1), data(:, 2), data(:, 3), 'x', ...
                'MarkerSize', 4, 'LineWidth', 2)
            set(gca, 'xscale', 'log');
            set(gca, 'yscale', 'log');

            hold on
            % Cahn et al. (1992) eq. 8
            if R == 0.12
                Rfit = 0.1;
            else
                Rfit = 1.5;
            end

            f = @(a, b) 1e-3 * (a * dT.^(- 1/3) / Rfit ...
                + b * (Rfit * dT).^(- 2));

            f1 = f(6.61, 2.26);
            f2 = f(8.57, 5.75);
            h = loglog(dT, [f1; f2], 'k', 'linewidth', 2);
            set(h, {'color'}, num2cell(jet(2), 2));

            file = importdata(mcdata, ',', 6);
            data = file.data;
            tf = data(:, 4);
            rf = data(:, 5);
            rp1 = data(:, 6);
            rp2 = data(:, 7);
            rp = data(:, 8);
            rt = data(:, 9);

            idx_pore = rp > 0;
            idx_throat = rt > 0;
            idx_crevice = rp1 > 0;
            idx_film = tf > 0;
            P = size(data, 1);
            numliquid = sum(idx_film);

            Tpore = zeros(P, 1); % temperature threshold for pore
            Tthroat = zeros(P, 1); % temperature threshold for throat
            Tcrevice = zeros(P, 1); % temperature threshold for crevice
            Tfilm = zeros(P, 1);

            Tfilm(idx_film) = undercooling.film(rf(idx_film), ...
                tf(idx_film), lscale);
            Tpore(idx_pore) = undercooling.pore(rp(idx_pore));
            Tthroat(idx_throat) = undercooling.pore(rt(idx_throat));
            Tcrevice(idx_crevice) = undercooling.crevice(rp1(idx_crevice), ...
                rp2(idx_crevice));

            Tc = max([Tcrevice, Tfilm], [], 2);
            Tc(Tcrevice <= 0) = 0;
            indc = Tc > 0;
            numcrevice = sum(indc);
            Tcrevicesort = sort(Tc(indc), 'descend');
            Tcrevicepct = (1:numcrevice)' / numliquid;
            loglog(Tcrevicesort, Tcrevicepct, 'k--', ...
                'LineWidth', 2, 'MarkerSize', 4);

            Tt = max([Tthroat, Tpore, Tfilm], [], 2);
            indt = Tt > 0;
            numthroat = sum(indt);
            Tthroatsort = sort(Tt(indt), 'descend');
            Tthroatpct = (1:numthroat)' / numliquid;
            loglog(Tthroatsort, Tthroatpct, 'r-.', ...
                'LineWidth', 2, 'MarkerSize', 4);

            xlabel('undercooling (K)', 'fontsize', 20)
            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)

            if R == 0.12
                legend('graphitized data', 'sc', 'fcc', 'crevice total', ...
                'throat total')
                xlim([0.1, 50])
                ylim([0.01, 1])
            elseif R == 2.5
                legend('polystyrene data', 'sc', 'fcc', 'crevice total', ...
                'throat total')
                xlim([0.01, 50])
                ylim([1e-4, 1])
            end

        end

        function millville
            % Calculate the matric potential of Millville silt loam
            % and compare the results with experiment measurements.

            % sand, silt, and clay
            % m = [0.33, 0.49, 0.18] * 1e-3;
            % rho = 2650;
            % V = m / rho;
            % d = 2 .^ [- 4, - 8, - 10] * 1e-3;
            % n = V ./ (pi / 6 * d .^ 3);
            % S = sum(n * pi .* d .^ 2);
            % p = n / sum(n); % number probability

            figure
            expdata = csvread('data/soil/or1999_millville.csv');
            loglog(expdata(:, 1), expdata(:, 2), 'ko', 'MarkerSize', 6)
            hold on
            xlim([9e3, 1e9])
            ylim([0.05, 1])

            % mcdata ='data/mc/mc_sc_05_22_2019_15_14.txt' % test simple cubic
            mcdata = 'data/mc/mc_millville_05_22_2019_19_51.txt';
            % mcdata='mc_millville_06_19_2019_16_35.txt';
            file = importdata(mcdata, ',', 6);
            data = file.data * 1e-6;
            % data = file.data * 0.0155e-6;
            % no throat data needed
            tf = data(:, 4);
            rf = data(:, 5);
            rp1 = data(:, 6);
            rp2 = data(:, 7);
            % rp = data(:, 8);
            rt = data(:, 9);
            idx_f = rf > 0;
            idx_c = rp1 > 0;
            % idx_p = rp > 0;
            idx_t = rt > 0;
            numliquid = sum(idx_f);

            P = size(data, 1);

            lscale = 0.4;
            param = jsondecode(fileread('param.json'));
            ldnot = str2double(param.film.thickness.value) * lscale;
            pnot = str2double(param.film.pressure.value);
            gamma = str2double(param.h2o.surface_tension.value);
            beta = 3;

            % film matric potential
            Pfilm = zeros(P, 1);
            Pcrevice = zeros(P, 1);
            % Ppore = zeros(P, 1);
            Pthroat = zeros(P, 1);
            Pfilm(idx_f) = pnot * (ldnot ./ tf(idx_f)).^beta ...
                - 2 * gamma ./ rf(idx_f);
            Pcrevice(idx_c) = gamma * (1 ./ rp1(idx_c) + 1 ./ rp2(idx_c));
            % Ppore(idx_p) = gamma * 2 ./ rp(idx_p);
            Pthroat(idx_t) = gamma * 2 ./ rt(idx_t);

            % Pfilmp = Pfilm(Pfilm > 0);
            % Pfilmsort = sort(Pfilmp, 'descend');
            % Pfilmpct = (1:length(Pfilmsort)) / numliquid;
            % loglog(Pfilmsort, Pfilmpct, 'r', 'linewidth', 2)

            % Pcrevicep = Pcrevice(Pcrevice > 0);
            % Pcrevicesort = sort(Pcrevicep, 'descend');
            % Pcrevicepct = (1:length(Pcrevicesort)) / numliquid;
            % loglog(Pcrevicesort, Pcrevicepct, 'b', 'linewidth', 2)

            Pliq1 = max([Pfilm, Pcrevice], [], 2);
            ind1 = Pliq1 > 0;
            Psort1 = sort(Pliq1(ind1), 'descend');
            Ppct1 = (1:length(Psort1))' / numliquid;
            loglog(Psort1, Ppct1, 'b-', 'linewidth', 2);

            % Pporep = Ppore(Ppore > 0);
            % Pporesort = sort(Pporep, 'descend');
            % Pporepct = (1:length(Pporesort)) / numliquid;
            % loglog(Pporesort, Pporepct, 'g', 'linewidth', 2)

            % Pliq2 = max([Pfilm, Ppore], [], 2);
            % ind2 = Pliq2 > 0;
            % Psort2 = sort(Pliq2(ind2), 'descend');
            % Ppct2 = (1:length(Psort2))' / numliquid;
            % loglog(Psort2, Ppct2, 'g+-', 'linewidth', 2)

            % Pthroatp = Pthroat(Pthroat > 0);
            % Pthroatsort = sort(Pthroatp, 'descend');
            % Pthroatpct = (1:length(Pthroatsort)) / numliquid;
            % loglog(Pthroatsort, Pthroatpct, 'm-', 'linewidth', 2)

            Pliq3 = max([Pfilm, Pthroat], [], 2);
            ind3 = Pliq3 > 0;
            Psort3 = sort(Pliq3(ind3), 'descend');
            Ppct3 = (1:length(Psort3))' / numliquid;
            loglog(Psort3, Ppct3, 'm--', 'linewidth', 2)

            legend('measurements', 'crevice+film', 'throat+pore+film')
            xlabel('matric potential (Pa)', 'fontsize', 20)
            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)
        end

        function [Ttotal, Sl] = total(mcdata, rscale, lscale)
            % Calculate the total undercooling from the MC data. The
            % parameter rscale is the scale of the median radius, and lscale is
            % for the film thickness.

            if nargin < 3
                lscale = 1;

                if nargin < 2
                    rscale = 1;
                end

            end

            file = importdata(mcdata, ',', 6);
            data = file.data * rscale;
            tf = data(:, 4);
            rf = data(:, 5);
            rp1 = data(:, 6);
            rp2 = data(:, 7);
            rp = data(:, 8);
            rt = data(:, 9);

            idx_pore = rp > 0;
            idx_throat = rt > 0;
            idx_crevice = rp1 > 0;
            idx_film = tf > 0;
            P = size(data, 1);
            numliquid = sum(idx_film);

            Tpore = zeros(P, 1); % temperature threshold for pore
            Tthroat = zeros(P, 1); % temperature threshold for throat
            Tcrevice = zeros(P, 1); % temperature threshold for crevice
            Tfilm = zeros(P, 1);

            Tfilm(idx_film) = undercooling.film(rf(idx_film), tf(idx_film), ...
                lscale);
            Tpore(idx_pore) = undercooling.pore(rp(idx_pore));
            Tthroat(idx_throat) = undercooling.pore(rt(idx_throat));
            Tcrevice(idx_crevice) = undercooling.crevice(rp1(idx_crevice), ...
                rp2(idx_crevice));

            Tt = max([Tcrevice, Tthroat, Tpore, Tfilm], [], 2);
            indt = Tt > 0;
            num = sum(indt);
            Ttotal = sort(Tt(indt), 'descend');
            Sl = (1:num)' / numliquid;

        end

        function compare_wetting
            % COMPARE_WETTING Plot the effects of different wetting parameters
            % on the undercooling curve.

            % the mono-sized case
            mcdata = 'data/mc/mc_mono_05_23_2019_18_01.txt';

            cmap = pmkmp(7, 'CubicL');

            n = 5;
            subplot 121

            box on
            hold on

            for k = 1:6

                lscale = k - 1;
                lc1{k} = sprintf('$\\lambda_0=\\qty{%0.1f}{\\nano\\m}$', ...
                    3.5 * (k - 1));

                [Ttotal, Sl] = freezing.total(mcdata, 1, lscale);
                plot(Ttotal(1:n:end), Sl(1:n:end), 'color', cmap(k, :), ...
                    'LineWidth', 2, 'MarkerSize', 4);

            end

            xlim([- inf, 1e3])
            ylim([1e-3, 1])

            legend(lc1, 'interpreter', 'latex', 'location', 'best')

            xlabel('undercooling (K)', 'fontsize', 20)
            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)

            text(0.1, 5e-3, '(a)');

            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

            subplot 122

            box on
            hold on

            k = 0;

            for rscale = 1:2:11
                k = k + 1;
                lc2{k} = sprintf('$r_m=\\qty{%d}{\\micro\\m}$', rscale);
                [Ttotal, Sl] = freezing.total(mcdata, rscale);
                plot(Ttotal(1:n:end), Sl(1:n:end), 'color', cmap(k, :), ...
                    'LineWidth', 2, 'MarkerSize', 4);

            end

            xlim([- inf, 1e3])
            ylim([1e-3, 1])

            legend(lc2, 'interpreter', 'latex', 'location', 'best')

            xlabel('undercooling (K)', 'fontsize', 20)
            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)

            text(0.05, 5e-3, '(b)')

            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

        end

        function compare_salt
            % COMPARE_SALT Plot the effects of chlorides and perchlorates with
            % different initial concentrations on the undercooling curve.
            % For low concentrations, the density of the liquid is approximately
            % unchanged.

            % the mono-sized case
            mcdata = 'data/mc/mc_mono_05_23_2019_18_01.txt';

            n = 10;

            % salts with wt%
            wt = [0.1, 1, 10] * 1e-3;

            figure('Position', [100, 100, 1000, 1000]);

            % chlorides

            % pure water
            [Ttotal, Sl] = freezing.total(mcdata, 1, 0);

            subplot 321

            plot(Ttotal(1:n:end), Sl(1:n:end), 'k', 'LineWidth', 2);
            box on
            hold on

            m = undercooling.wt2molal(wt, 'NaCl');
            cmap = pmkmp(5, 'CubicL');

            for i = 1:length(m)
                dT = undercooling.freezing(m(i) ./ Sl(1:n:end), 'NaCl');
                plot(Ttotal(1:n:end) + dT, Sl(1:n:end), 'color', cmap(i, :), ...
                    'LineWidth', 2, 'MarkerSize', 10);
            end

            title('\ch{NaCl}, $\lambda_0=0$, $r_m=\qty{1}{\um}$', ...
                'interpreter', 'latex', 'fontweight', 'normal')
            legend('pure water', '0.01 wt%', ...
                '0.1 wt%', '1 wt%', ...
                'location', 'best')

            text(0.1, 1e-2, '(a)')

            xlim([0.05, 10])
            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)

            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

            subplot 322

            plot(Ttotal(1:n:end), Sl(1:n:end), 'k', 'LineWidth', 2);
            box on
            hold on

            m = undercooling.wt2molal(wt, 'MgCl2');

            for i = 1:length(m)
                dT = undercooling.freezing(m(i) ./ Sl(1:n:end), 'MgCl2');
                plot(Ttotal(1:n:end) + dT, Sl(1:n:end), 'color', cmap(i, :), ...
                    'LineWidth', 2, 'MarkerSize', 10);
            end

            title('\ch{MgCl2}, $\lambda_0=0$, $r_m=\qty{1}{\um}$', ...
                'interpreter', 'latex', 'fontweight', 'normal')
            legend('pure water', '0.01 wt%', ...
                '0.1 wt%', '1 wt%', ...
                'location', 'best')

            xlim([0.05, 10])

            text(0.1, 1e-2, '(b)')

            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

            % perchlorates

            % pure water with lambda_0 = 9 nm and rm = 1 μm
            [Ttotal0, Sl0] = freezing.total(mcdata, 1, 0);
            [Ttotal, Sl] = freezing.total(mcdata, 1, 9/3.5);

            subplot 323

            plot(Ttotal0(1:n:end), Sl0(1:n:end), 'k--', ...
                Ttotal(1:n:end), Sl(1:n:end), 'k', 'LineWidth', 2);
            box on
            hold on

            m = undercooling.wt2molal(wt, 'MgPer');

            for i = 1:length(m)
                dT = undercooling.freezing(m(i) ./ Sl(1:n:end), 'MgPer');
                plot(Ttotal(1:n:end) + dT, Sl(1:n:end), 'color', cmap(i, :), ...
                    'LineWidth', 2, 'MarkerSize', 10);
            end

            title('\ch{Mg(ClO4)2}, $\lambda_0=\qty{9}{\nm}$, $r_m=\qty{1}{\um}$', ...
                'interpreter', 'latex', 'fontweight', 'normal')
            legend('$\lambda_0=0$', 'pure water', '0.01 wt%', '0.1 wt%', '1 wt%', ...
                'location', 'best')

            xlim([0.05, 50])
            ylim([1e-2, 1])

            text(0.1, 0.04, '(c)')

            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)
            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

            subplot 325

            plot(Ttotal0(1:n:end), Sl0(1:n:end), 'k--', ...
                Ttotal(1:n:end), Sl(1:n:end), 'k', 'LineWidth', 2);
            box on
            hold on

            m = undercooling.wt2molal(wt, 'CaPer');

            for i = 1:length(m)
                dT = undercooling.freezing(m(i) ./ Sl(1:n:end), 'CaPer');
                plot(Ttotal(1:n:end) + dT, Sl(1:n:end), 'color', cmap(i, :), ...
                    'LineWidth', 2, 'MarkerSize', 10);
            end

            title('\ch{Ca(ClO4)2}, $\lambda_0=\qty{9}{\nm}$, $r_m=\qty{1}{\um}$', ...
                'interpreter', 'latex', 'fontweight', 'normal')
            legend('$\lambda_0=0$', 'pure water', '0.01 wt%', '0.1 wt%', '1 wt%', ...
                'location', 'best')

            xlim([0.05, 50])
            ylim([1e-2, 1])

            text(0.1, 0.04, '(e)')

            xlabel('$\Delta T$ (K)', 'interpreter', 'latex', 'fontsize', 20)
            ylabel('$S_l$', 'interpreter', 'latex', 'fontsize', 20)
            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

            % pure water with lambda_0 = 9 nm and rm = 50 μm
            [Ttotal0, Sl0] = freezing.total(mcdata, 50, 0);
            [Ttotal, Sl] = freezing.total(mcdata, 50, 9/3.5);

            subplot 324

            plot(Ttotal0(1:n:end), Sl0(1:n:end), 'k--', ...
                Ttotal(1:n:end), Sl(1:n:end), 'k', 'LineWidth', 2);
            box on
            hold on

            m = undercooling.wt2molal(wt, 'MgPer');

            for i = 1:length(m)
                dT = undercooling.freezing(m(i) ./ Sl(1:n:end), 'MgPer');
                plot(Ttotal(1:n:end) + dT, Sl(1:n:end), 'color', cmap(i, :), ...
                    'LineWidth', 2, 'MarkerSize', 10);
            end

            title('\ch{Mg(ClO4)2}, $\lambda_0=\qty{9}{\nm}$, $r_m=\qty{50}{\um}$', ...
                'interpreter', 'latex', 'fontweight', 'normal')
            legend('$\lambda_0=0$', 'pure water', '0.01 wt%', '0.1 wt%', '1 wt%', ...
                'location', 'best')

            xlim([1e-3, 50])
            ylim([1e-2, 1])

            text(0.0025, 0.05, '(d)')

            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')

            subplot 326

            plot(Ttotal0(1:n:end), Sl0(1:n:end), 'k--', ...
                Ttotal(1:n:end), Sl(1:n:end), 'k', 'LineWidth', 2);
            box on
            hold on

            m = undercooling.wt2molal(wt, 'CaPer');

            for i = 1:length(m)
                dT = undercooling.freezing(m(i) ./ Sl(1:n:end), 'CaPer');
                plot(Ttotal(1:n:end) + dT, Sl(1:n:end), 'color', cmap(i, :), ...
                    'LineWidth', 2, 'MarkerSize', 10);
            end

            title('\ch{Ca(ClO4)2}, $\lambda_0=\qty{9}{\nm}$, $r_m=\qty{50}{\um}$', ...
                'interpreter', 'latex', 'fontweight', 'normal')
            legend('$\lambda_0=0$', 'pure water', '0.01 wt%', '0.1 wt%', '1 wt%', ...
                'location', 'best')

            xlim([1e-3, 50])
            ylim([1e-2, 1])

            text(0.0025, 0.05, '(f)')

            xlabel('$\Delta T$ (K)', 'interpreter', 'latex', 'fontsize', 20)
            set(gca, 'xscale', 'log')
            set(gca, 'yscale', 'log')
        end

    end

end
