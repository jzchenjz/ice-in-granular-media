classdef sediment
    % Calculate and show the properties of the sediments.
    % TODO: 1. Porosity calculation is too coarse, L is changing

    methods (Static)

        % ----- Fit the soil samples ----- %

        function [mu, sigma] = distfit(d, cvf, varargin)
            % Fit the cumulative volumetric fractions data of soil or
            % sediment samples to a log-normal distribution for the diameter.
            % The diameters are in micron.

            if ~isempty(varargin)
                mu0 = varargin{1};
                sigma0 = varargin{2};
            else
                mu0 = 1;
                sigma0 = 1;
            end

            if size(d, 1) == 1
                d = d';
            end

            if size(cvf, 1) == 1
                cvf = cvf';
            end

            if max(cvf) > 2
                error('distfit: cumulative fraction must be less than unity!')
            end

            fo = fitoptions('Method', 'NonlinearLeastSquares', 'StartPoint', ...
                [mu0, sigma0]);

            vcdf = @(mu, sigma, x) erfc((mu + 3 * sigma^2 - log(x)) ...
                / sigma / sqrt(2)) / 2;

            ft = fittype(vcdf, 'options', fo);
            f = fit(d, cvf, ft);

            figure
            mu = f.mu;
            sigma = f.sigma;

            % plot
            dmin = min(d) / 10;
            dmax = max(d) * 5;
            dx = logspace(log10(dmin), log10(dmax));
            semilogx(d, cvf, 'ro', d, vcdf(mu, sigma, d), 'k-', ...
                dx, vcdf(mu, sigma, dx), 'r--', 'linewidth', 2)
            xlabel('$d$', 'interpreter', 'latex');
            ylabel('cumulative volumetric fractions');
            legend('data', ['CDF, \mu=', num2str(mu, '%5.2f'), ', \sigma=', ...
                        num2str(sigma, '%5.2f')], 'location', 'best')
        end

        function [mu, sigma] = jsc
            % Fit the volumetric fractions data of JSC-Mars-1 from
            % Dinwiddie, C. and Sizemore, H. (2008).

            if nargin < 1
                data = load('data/sediments/jsc.csv');
            end

            r = data(:, 1) / 2;
            vf = cumsum(data(:, 2));

            [mu, sigma] = sediment.distfit(r', vf');
        end

        function sigmai = igsd(r)
            % Calculate the inclusive graphic standard deviation of the
            % particles:
            % sigma i = (phi84 - phi16) / 4 + (phi95 - phi5) / 6.6
            % The particles size can be in μm or mm, radii or diameters.

            phi =- log(r) / log(2);
            phi84 = prctile(phi, 84);
            phi16 = prctile(phi, 16);
            phi95 = prctile(phi, 95);
            phi5 = prctile(phi, 5);

            sigmai = (phi84 - phi16) / 4 + (phi95 - phi5) / 6.6;

            if sigmai < 0.35
                disp('Very well sorted!')
            elseif sigmai < 0.5
                disp('Well sorted!')
            elseif sigmai < 1
                disp('Moderately sorted!')
            elseif sigmai < 2
                disp('Poorly sorted!')
            elseif sigmai < 4
                disp('Very poorly sorted!')
            else
                disp('Extremely poorly sorted!')
            end

        end

        % ----- Read the sediments ----- %

        function particles = load(sedfile)
            % Read in the data of the particles, and return the ids and
            % (x, y, z, r) of the particles.

            if nargin < 1
                sedfile = 'data/mono-40000-02-20-2019-18-11.mat';
            end

            sed = load(sedfile);
            particles = sed.particles(:, 1:4);
        end

        function [sigma, mu, sortedness] = variance(particles)
            % Calculate the variance and sortedness of the particles.

            r = particles(:, 4); % radii of particles in μm

            m = mean(r);
            v = var(r);

            if v > 0
                s = 1 + v / m^2;
                mu = log(m) - log(s) / 2;
                sigma = sqrt(log(s));
            end

            sortedness = sediment.igsd(r);

        end

        function distribution(particles, rmax)
            % Calculate and plot the statistical properties of
            % the sediment particles.
            % The JSC-Mars-1 parameters are (3.19, 0.75).

            r = particles(:, 4); % radii of particles in μm

            m = mean(r);
            v = var(r);

            if nargin < 2
                rmax = 5 * m;
            end

            if v > 0
                s = 1 + v / m^2;
                mu = log(m) - log(s) / 2;
                sigma = sqrt(log(s));
                pdi = v / m^2; % polydispersity index
                disp(['mu: ', num2str(mu)]);
                disp(['sigma: ', num2str(sigma)]);
                disp(['pdi: ', num2str(pdi)])
                r(r > rmax) = [];
                [fi, ri] = ksdensity(r);
                fi(ri < 0) = [];
                ri(ri < 0) = [];
                plot(ri, fi, 'linewidth', 2)
                hold on
                % compare with a log-normal pdf
                rr = linspace(min(r), max(r), 200);
                p = 1 / sqrt(2 * pi) ./ rr / sigma ...
                    .* exp(- (log(rr) - mu).^2/2 / sigma^2);
                plot(rr, p, 'r')
                xlim([0, rmax])
                xlabel('R ($\mu$m)', 'interpreter', 'latex')
                ylabel('Frequency')
                title(['\mu=', num2str(mu, '%5.2f'), ', \sigma=', ...
                        num2str(sigma, '%5.2f')]);
            end

        end

        function selected = selection(sedfile, z, k)
            % Find the ids of the particles that is used in MC
            % simulations.

            if nargin < 3
                k = 40;
            end

            particles = sediment.load(sedfile);

            r = particles(:, 4);
            meanR = mean(r);
            maxR = max(r);

            testR = k * meanR;
            meanxy = mean(particles(:, 1:2));

            % in mono-dispersed case, one maxR gives wrong crevice
            highid = particles(:, 3) < z + 2 * maxR; %
            lowid = particles(:, 3) > z - 2 * maxR;
            inid = vecnorm(particles(:, 1:2) - meanxy, 2, 2) < testR;

            selected = particles(highid & lowid & inid, :);
        end

        function xsection(sedfile, z, k)
            % Make a plot of a cross-section of the sediments.

            if nargin < 3
                k = 40;
            end

            particles = sediment.load(sedfile);
            zpos = particles(:, 3);
            r = particles(:, 4);
            hpos = particles(:, 1:2);
            teal = [0, 0.5, 0.5];
            figure
            zid = abs(zpos - z) <= r;
            dz = zpos(zid) - z;
            zr = sqrt(r(zid).^2 - dz.^2);
            zhpos = hpos(zid, :);
            title(['z = ', num2str(z), ' $\mu$m'], 'interpreter', 'latex')
            viscircles(zhpos, zr, 'Color', teal);
            hold on
            meanR = mean(r);
            testR = k * meanR;
            meanxy = mean(particles(:, 1:2)); t = viscircles(meanxy, testR, 'Color', 'r', 'LineStyle', '--');
            xlabel('$x$ ($\mu$m)', 'interpreter', 'latex')
            ylabel('$y$ ($\mu$m)', 'interpreter', 'latex')
            axis equal
        end

        function porosity(sedfile)
            % Read in the sediments data and estimate the porosity
            % at different heights.

            particles = sediment.load(sedfile);
            zpos = particles(:, 3);
            r = particles(:, 4);

            zbottom = max(min(zpos), max(r));
            ztop = max(zpos) - max(r);
            z = linspace(zbottom, ztop);
            phi = zeros(size(z));
            L = 4000;

            for i = 1:length(z)
                zid = abs(zpos - z(i)) <= r;
                dz = zpos(zid) - z(i);
                zr = sqrt(r(zid).^2 - dz.^2);
                phi(i) = 1 - sum(zr.^2) * pi / L^2;
            end

            % plot the profile
            plot(z, phi, 'linewidth', 2);
            xlabel('$z$ ($\mu$m)', 'interpreter', 'latex')
            ylabel('$\phi$', 'interpreter', 'latex')
        end

    end

end
