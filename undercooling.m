classdef undercooling
    % Calculate the effects of undercooling caused by colligative effects
    % and film. We consider five different solutes, including chlorides NaCl,
    % MgCl2 and KCl, and perchlorates including Mg(ClO4)2 and Ca(ClO4)2, which
    % are denoted as MgPer and CaPer.

    methods (Static)

        function wt = molal2wt(m, solute)
            % Convert the molality of a solute to weight percent.

            solutelist = {'NaCl', 'MgCl2', 'KCl', 'MgPer', 'CaPer'};

            if ~any(strcmpi(solutelist, solute))
                error('molal2wt: Not applicable solute!');
            end

            param = jsondecode(fileread('param.json'));
            Ms = str2double(param.salt.(solute).mass.value);
            wt = Ms * m ./ (Ms * m + 1);
        end

        function m = wt2molal(wt, solute)
            % Convert the weight percent of a solute to molality.

            solutelist = {'NaCl', 'MgCl2', 'KCl', 'MgPer', 'CaPer'};

            if ~any(strcmp(solutelist, solute))
                error('wt2molal: Not applicable solute!')
            end

            if wt > 1
                error('wt2molal: Weight percent should be less than 1!')
            end

            param = jsondecode(fileread('param.json'));
            Ms = str2double(param.salt.(solute).mass.value);
            m = 1 / Ms ./ (1 ./ wt - 1);
        end

        % ----- Colligative effects ----- %

        function wt = depression(T, solute)
            % Calculate the freezing point depression of water
            % with the presence of chlorides with given weight percent.

            if nargin < 2
                solute = 'MgCl2';
            end

            solutelist = {'NaCl', 'KCl', 'MgCl2'};

            if ~any(strcmpi(solutelist, solute))
                error('depression: No applicable solute!');
            end

            param = jsondecode(fileread('param.json'));
            H = str2double(param.h2o.fusion_enthalpy.value);
            Tm = str2double(param.h2o.freezing.value);
            R = 8.3144598;

            aw = exp(H / R * (1 / Tm - 1 ./ T));

            book = readtable('data/colligative.csv');
            B = flipud(book.(solute)(2:end));

            f = @(a) polyval(B, a);
            m = f(aw);
            wt = undercooling.molal2wt(m, solute);
        end

        function [dT, aw] = chloride(m, solute)
            % Calculate the freezing point depression of water
            % with the presence of common chlorides (NaCl, KCl or MgCl2) with
            % given molality.
            % The eutectic temperatures are:
            % -21.1 degree C for NaCl, -10.6 degree C for KCl, and -33 degree C
            % for MgCl2

            if nargin < 2
                solute = 'MgCl2';
            end

            solutes = {'NaCl', 'KCl', 'MgCl2'};

            if ~any(strcmpi(solutes, solute))
                error('chloride: No applicable solute!');
            end

            param = jsondecode(fileread('param.json'));
            H = str2double(param.h2o.fusion_enthalpy.value);
            Tm = str2double(param.h2o.freezing.value);
            R = 8.3144598;

            % concentration below eutectic concentration
            meu = str2double(param.salt.(solute).meu.value);
            m(m > meu) = meu;

            aw = zeros(size(m));

            book = readtable('data/colligative.csv');
            B = flipud(book.(solute)(2:end));

            for i = 1:numel(m)

                if m(i)
                    f = @(a) polyval(B, a) - m(i);
                    aw(i) = fzero(f, [0, 1]);
                else
                    aw(i) = 1;
                end

            end

            dT = Tm - 1 ./ (1 / Tm - R / H * log(aw));
        end

        function dT = perchlorate(m, solute)
            % Calculate the undercooling of Mg(ClO4)2 and Ca(ClO4)2
            % using the fit from the freezing points by Pestrova et al. (2005).
            % Note that this is a polynomial fit of the freezing points. The
            % results can be compared against other predictions from the revised
            % Pitzer model, e.g., Toner, Catling and Light (2015), and Toner and
            % Catling (2016). Some differences may arise due to the temperature
            % change.

            if nargin < 2
                solute = 'MgPer';
            end

            solutelist = {'MgPer', 'CaPer'};

            if ~any(strcmpi(solutelist, solute))
                error('perchlorate: No applicable solute!');
            end

            if strcmpi(solute, 'MgPer')
                % Teu = 204.55; % eutectic T
                meu = 3.48; % eutectic molality
                % fit constants: liquidus T from molality < 3.48, ensuring T =
                % 273.15 K at m = 0 and T = Teu at meu, using polyfix
                % https://www.mathworks.com/matlabcentral/fileexchange/54207
                % p = polyfix(m, T + 273.15, 3, [0, meu], [273.15, Teu]);
                p = [- 1.8650, 4.4425, - 12.5866, 273.15];
            else
                % Teu = 198.55;
                meu = 4.20;
                p = [- 0.7373, 0.8484, - 8.3196, 273.15];
            end

            param = jsondecode(fileread('param.json'));
            Tm = str2double(param.h2o.freezing.value);

            % upper limit set as eutectic molality
            m(m > meu) = meu;
            dT = zeros(size(m));

            for i = 1:numel(m)
                dT(i) = Tm - polyval(p, m(i));
            end

        end

        function dT = freezing(m, solute)
            % Calculate the freezing point depression of water
            % with the presence of common solutes (NaCl, KCl or MgCl2) with
            % given molality.

            chlorides = {'NaCl', 'KCl', 'MgCl2'};
            perchlorates = {'MgPer', 'CaPer'};

            if any(strcmpi(chlorides, solute))
                [dT, ~] = undercooling.chloride(m, solute);
            elseif any(strcmpi(perchlorates, solute))
                dT = undercooling.perchlorate(m, solute);
            else
                error('freezing: No applicable solute!');
            end

        end

        function colligative(solute)
            % Plot the colligative effects of freezing point
            % depression with the presence of solute.

            if nargin < 1
                solute = 'MgCl2';
            end

            solutelist = {'NaCl', 'KCl', 'MgCl2'};

            if ~any(strcmpi(solutelist, solute))
                error('colligative: Not applicable solute!');
            end

            param = jsondecode(fileread('param.json'));
            H = str2double(param.h2o.fusion_enthalpy.value);
            Tm = str2double(param.h2o.freezing.value);
            R = 8.3144598;

            T = linspace(- 30, 0) + Tm;

            aw = exp(H / R * (1 / Tm - 1 ./ T));

            book = readtable('data/colligative.csv');
            B = flipud(book.(solute)(2:end));

            f = @(a) polyval(B, a);
            m = f(aw);
            wt = undercooling.molal2wt(m, solute);

            % plot the colligative effect
            plot(wt * 100, T - Tm, 'linewidth', 2)
            title(['Freezing point depression of ', solute])
            xlim([0, inf])
            xlabel('wt%')
            ylabel('Freezing point ({\circ}C)')
        end

        function electrolyte(solute)
            % Plot the undercooling of hydrate at 20 MPa with the
            % presence of salt.

            param = jsondecode(fileread('param.json'));
            H = str2double(param.hydrate.dissociation_heat.value);
            R = 8.3144598;
            Tm = 292;
            n = 6;

            if nargin < 1
                solute = 'NaCl';
            end

            solutelist = {'NaCl', 'KCl', 'MgCl2'};

            if ~any(strcmpi(solutelist, solute))
                error('electrolyte: Not applicable solute!');
            end

            m = 0:0.1:0.6;

            aw = zeros(size(m));

            book = readtable('data/colligative.csv');
            B = flipud(book.(solute)(2:end));

            for i = 1:numel(m)

                if m(i)
                    f = @(a) polyval(B, a) - m(i);
                    aw(i) = fzero(f, [0, 1]);
                else
                    aw(i) = 1;
                end

            end

            dT = Tm - 1 ./ (1 / Tm - n * R / H * log(aw));

            % plot the undercooling
            plot(m, dT, 'k', 'linewidth', 2)
            xlim([0, 0.6])
            xlabel('m_s')
            ylabel('\Delta T (K)')
        end

        % ----- Undercooling effects ----- %

        function Tfilm = film(r, d, lscale)
            % Calculate the undercooling below which the film is
            % frozen. The inputs r and d are in μm, and lscale is a scaling
            % factor for the lambda.

            if nargin < 3
                lscale = 1;
            end

            param = jsondecode(fileread('param.json'));
            Tm = str2double(param.h2o.freezing.value);
            rho = str2double(param.ice.density.value);
            lat = str2double(param.ice.latent_heat.value);
            lambda = str2double(param.film.thickness.value) * lscale;
            P0 = str2double(param.film.pressure.value);
            gamma = str2double(param.ice.surface_tension.value); % water-ice
            beta = 3;

            Tfilm = zeros(size(r));
            idx = r > 0;
            d = d(idx) / 1e6;
            r = r(idx) / 1e6;
            Tfilm(idx) = Tm / rho / lat * ...
                (P0 * (lambda ./ d).^beta - 2 * gamma ./ r);

            % in case that the points are too far away from the particles,
            % the undercooling should be approximately zero
            Tfilm(Tfilm < 0) = 0;
        end

        function Tcrevice = crevice(r1, r2)
            % Calculate the undercooling below which the unduloid
            % crevice is frozen. The inputs are the two principal radii in
            % μm, with r1 (minor radius) > 0 and r2 (minor - major) < 0.

            param = jsondecode(fileread('param.json'));
            Tm = str2double(param.h2o.freezing.value);
            rho = str2double(param.ice.density.value);
            lat = str2double(param.ice.latent_heat.value);
            gamma = str2double(param.ice.surface_tension.value);

            Tcrevice = zeros(size(r1));
            idx = r1 > 0;
            r1 = r1(idx) / 1e6;
            r2 = r2(idx) / 1e6;

            Tcrevice(idx) = gamma * Tm / rho / lat * (1 ./ r1 + 1 ./ r2);
            Tcrevice(Tcrevice < 0) = 0;
        end

        function Tpore = pore(r)
            % Calculate the undercooling below which the pore is frozen.
            % The input is in μm.

            param = jsondecode(fileread('param.json'));
            Tm = str2double(param.h2o.freezing.value);
            rho = str2double(param.ice.density.value);
            lat = str2double(param.ice.latent_heat.value);
            gamma = str2double(param.ice.surface_tension.value);

            Tpore = zeros(size(r));
            idx = r > 0;
            r = r(idx) / 1e6;

            Tpore(idx) = 2 * gamma * Tm / rho / lat ./ r;
        end

    end

end
