# Ice in granular media

Matlab scripts to simulate the undercooling of ice in granular media, for the publication [_A Monte Carlo Approach to Approximating the Effects of Pore Geometry on the Phase Behavior of Soil Freezing_](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2020MS002117). The functions are organized in serveral classes, including:

- `mc.m`. Perform Monte Carlo simulations given the synthetic sediment model (in `.mat` format containing a four-column array of particle positions and radii), number of test points, simulation name, scale of simulation region, and provide some analysis tools.
- `grow.m`. Calculate the maximum inscribed sphere between particles encompassing the test point using a 'nudge-and-bulge' algorithm, as well as the Apollonian circle for the throat and crevice calculation.
- `sediment.m`. Some tools to analyze the synthetic sediment model.
- `undercooling.m`. Use the Gibbs-Thomson equation to calculate the undercoolings caused by curvatures, and calculate the colligative effect of salts using the ZSR model.
- `freezing.m`. Compare the results against experiments and field measurements.
- `bubbleplot3.m`. A script by [Peter Bodin](https://www.mathworks.com/matlabcentral/fileexchange/8231-bubbleplot3) for easier visualization of multiple spheres, used in `grow.m`.
- `line_fewer_markers.m`. A script by [Massimo Ciacci](https://www.mathworks.com/matlabcentral/fileexchange/42560-line_fewer_markers) for plotting curves with better control of markers.

Parameters for calculations are in:
- `data/colligative.xlsx`. An Excel file contains all ZSR coefficients used to calculate the undercooling of chlorides.
- `param.json`. A file contains all other relevant parameters used in the simulation.
- `data/soil`. Some digitized measurements from literatures we compare our simulation results against.

Examples of simulated results are in `data/mc`. 
