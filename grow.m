classdef grow
    % Grow a spherical ice or hydrate inside the sediment particles.

    methods (Static)

        % ----- Analysis tools ----- %

        function visualize(particles)
            % Plot particles.

            px = particles(:, 1);
            py = particles(:, 2);
            pz = particles(:, 3);
            pr = particles(:, 4);
            figure
            bubbleplot3(px, py, pz, pr)
            shading interp;
            camlight right;
            lighting phong;
            xlabel('x', 'fontsize', 20)
            ylabel('y', 'fontsize', 20)
            zlabel('z', 'fontsize', 20)
            view(60, 30);
        end

        function viewpoint(particles, s)
            % Overlay the sphere s on the plot of nearby particles.

            k = min(8, size(particles, 1));

            [ids, ~] = grow.near(s, particles, k);
            rs = particles(ids, 4);

            if length(s) == 3
                r = min(rs) / 10;
            else
                r = s(4);
            end

            grow.visualize(particles(ids, :));
            hold on
            bubbleplot3(s(1), s(2), s(3), r, [1, 0, 0])
        end

        % ----- Particle operations ----- %

        function d = distance(s, ps)
            % Calculate the distances from the sphere s to particles
            % ps. The sphere can be only a point.

            if numel(s) == 3 % s is only a point, not a sphere
                s(4) = 0;
            end

            dmin = min(ps(:, 4)) / 1e5; % cutting threshold

            d = vecnorm(ps(:, 1:3) - s(1:3), 2, 2) - ps(:, 4) - s(4);
            d((d < 0) & (d >- dmin)) = 0;
        end

        function pnew = stack(s, ps)
            % Add the sphere s to particles ps. The sphere can be only a
            % point.

            if numel(s) == 3 % s is only a point, not a sphere
                s(4) = 0;
            end

            pnew = [ps; s];
        end

        function flag = cut(s, ps)
            % Determine if a sphere s is cutting any particles ps.

            d = grow.distance(s, ps);

            flag = any(d < 0);

        end

        function [ids, dists] = near(s, particles, k)
            % Find the nearest k particles from the test sphere s.

            if nargin < 3
                k = min(20, size(particles, 1)); % twenty nearest
            end

            % ids and distances of the closest particles
            d = grow.distance(s, particles);
            [dists, ids] = sort(d);
            ids = ids(1:k);
            dists = dists(1:k);
        end

        % ----- Geometry operations ----- %

        function h = height(pos, pos1, pos2)
            % Calculate the distance from the point pos to a line passing
            % through pos1 and pos2.

            u = pos - pos1;
            v = pos - pos2;
            w = pos1 - pos2; % we do not have zero-length w
            h = norm(cross(u, v)) / norm(w);
        end

        function R = project(pos1, pos2, pos3)
            % Calculate the transform matrix R that converts the center
            % points pos1, pos2, and pos3 into a horizontal plane. This is an
            % orthonormal matrix such that R * R' = I. For a 3D point in a row
            % vector v, its transformed position is vt = (R * v')' = v * R',
            % and to transform back, v = vt * R.

            % normal vector of the center plane
            n_vec = cross(pos1 - pos3, pos2 - pos3);
            n_vec = n_vec / norm(n_vec);
            z_hat = [0, 0, 1];
            v = cross(n_vec, z_hat);
            s = norm(v);
            c = dot(n_vec, z_hat);

            if s < 0.01 % almost horizontal
                R = eye(3);
            else
                % https://math.stackexchange.com/questions/180418
                Rv = [0, - v(3), v(2); v(3), 0, - v(1); - v(2), v(1), 0];
                R = eye(3) + Rv + Rv * Rv / (1 + c);
            end

        end

        function [posa, ra, posb, rb] = apollonius(pxy1, r1, pxy2, r2, pxy3, r3, showplot)
            % Find the circle tangent to three circles with centers
            % at pxy1, pxy2 and pxy3 with radii r1, r2, and r3.
            % Note that there are two circles, and the first one (posa, ra) is
            % with the larger radius, the second one (posb, rb) is smaller. In
            % case rb is negative, it is set to zero instead.
            %
            % Modified from Abhilash Harpale 2012
            % https://www.mathworks.com/matlabcentral/fileexchange/37403

            if nargin < 7
                showplot = false;
            end

            x1 = pxy1(1);
            y1 = pxy1(2);
            x2 = pxy2(1);
            y2 = pxy2(2);
            x3 = pxy3(1);
            y3 = pxy3(2);

            a2 = x1 - x2;
            a3 = x1 - x3;
            b2 = y1 - y2;
            b3 = y1 - y3;
            c2 = r1 - r2;
            c3 = r1 - r3;
            d2 = (x1 ^ 2 + y1 ^ 2 - r1 ^ 2 - x2 ^ 2 - y2 ^ 2 + r2 ^ 2) / 2;
            d3 = (x1 ^ 2 + y1 ^ 2 - r1 ^ 2 - x3 ^ 2 - y3 ^ 2 + r3 ^ 2) / 2;

            alpha1 = (b2 * d3 - b3 * d2) / (a3 * b2 - a2 * b3);
            alpha2 = (a2 * d3 - d2 * a3) / (a2 * b3 - a3 * b2);
            beta1 = (b3 * c2 - b2 * c3) / (a3 * b2 - a2 * b3);
            beta2 = (a3 * c2 - a2 * c3) / (a2 * b3 - a3 * b2);
            a = beta1 ^ 2 + beta2 ^ 2 - 1;
            b = 2 * (beta1 * (alpha1 - x1) + beta2 * (alpha2 - y1) - r1);
            c = (alpha1 - x1) ^ 2 + (alpha2 - y1) ^ 2 - r1 ^ 2;

            if b ^ 2 - 4 * a * c < 0
                ra = 0;
                rb = 0;
            else
                delta = sqrt(b ^ 2 - 4 * a * c);

                rx = (- b + delta * [1, - 1]) / 2 / a;

                if rx(1) + r1 >= 0 && rx(1) + r2 >= 0 && rx(1) + r3 >= 0
                    ra = rx(1);
                    rb = max(rx(2), 0);
                elseif rx(2) + r1 >= 0 && rx(2) + r2 >= 0 && rx(2) + r3 >= 0
                    ra = rx(2);
                    rb = 0;
                else % no solution
                    ra = 0;
                    rb = 0;
                end

            end

            posa = [alpha1 + beta1 * ra, alpha2 + beta2 * ra];
            posb = [alpha1 + beta1 * rb, alpha2 + beta2 * rb];

            if showplot
                % plot
                viscircles([pxy1; pxy2; pxy3], [r1; r2; r3], ...
                    'color', 'c', 'LineWidth', 1.5);
                hold on;
                viscircles(posa, ra, 'color', 'r', 'LineWidth', 1.5);
                viscircles(posb, rb, 'color', 'r', 'LineWidth', 1.5);
                plot(x1, y1, 'k*', x2, y2, 'k*', x3, y3, 'k*', ...
                    'markersize', 8)
                axis equal
                grid on
            end

        end

        function flag = cone(pos, p1, p2)
            % Check if the test point pos is inside the cone region between
            % two nearest particles.

            pos1 = p1(1:3);
            pos2 = p2(1:3);
            r1 = p1(4);
            r2 = p2(4);

            if r1 ~= r2
                l = norm(pos1 - pos2);
                theta = asin(abs(r1 - r2) / l); % cone angle
                q = pos1 + (pos2 - pos1) * r1 / (r1 - r2);
                thetap = acos(dot(q - pos, pos2 - pos1) / norm(q - pos) / l);

                if r1 > r2
                    flag = thetap < theta;
                else
                    flag = thetap > (pi - theta);
                end

            else
                flag = grow.height(pos, pos1, pos2) < r1;
            end

        end

        function flag = coplanar(ps)
            % Check if the points are in the same plane. This is done
            % by check the rank of the matrix of particles ps. If the rank is 2
            % or less, the points are coplanar.

            flag = rank(ps(2:end, 1:3) - ps(1, 1:3)) <= 2;
        end

        function snew = touch(s0, ps)
            % Find the sphere snew that touches four particles.

            options = optimset('Display', 'off');

            % touching four particles
            t4eq = @(x) grow.distance(x, ps);
            snew = fsolve(t4eq, s0, options);
        end

        % ----- Crevice growth scenario ----- %

        function [rp1, rp2, posx] = toroidal(pos, ps)
            % Calculate the toroidal approximation of the crevice
            % unduloid formed between two closest particles with the test point
            % pos on its surface. The point pos should be in the cone region
            % between ps.
            % In the plane passing through the particle centers and pos, the
            % poloidal center pos and radius of the poloidal circle rp1 (minor
            % radius) is calculated using the Apollonius ccp solution, which is
            % the first principal radius. The second principal radius rp2 is
            % approximated as the minimal distance from the poloidal circle to
            % the line connecting the two particle centers, which is the minor
            % radius minus the major (toroidal) radius.
            % Here we require positive r1 and negative rp2 for a convex bridge,
            % and if rp2 is positive, corresponding to a concave bridge, the
            % solution is abandoned and we report zeros for rp1 and rp2.

            pos1 = ps(1, 1:3);
            pos2 = ps(2, 1:3);
            r1 = ps(1, 4);
            r2 = ps(2, 4);

            R = grow.project(pos, pos1, pos2);
            pxy0 = pos * R';
            pxy1 = pos1 * R';
            pxy2 = pos2 * R';
            % find the circle, and the minor radius
            [pxy, rp1] = grow.apollonius(pxy1(1:2), r1, pxy2(1:2), r2, ...
                pxy0(1:2), 0);
            pxy(3) = pxy0(3);
            posx = pxy * R; % row vector

            % distance to the line connecting the two centers
            d = grow.height(posx, pos1, pos2); % major radius of the torus

            if rp1 >= d || rp1 == 0
                rp1 = 0;
                rp2 = 0;
            else
                rp2 = rp1 - d; % this should be negative
            end

        end

        % ----- Pore growth scenario ----- %

        function flag = contain(snew, pos, particles)
            % Check if the new sphere snew contains the point pos, and
            % does not cut any other particles.

            ids = grow.near(snew, particles);
            pnear = particles(ids, :);

            flag = grow.cut(pos, snew) && ~grow.cut(snew, pnear);
        end

        function snew = nudge(s, move)
            % Move the original sphere s with a step size move to a random
            % direction.

            theta = rand * 2 * pi;
            phi = acos(1 - 2 * rand);
            vec = [sin(phi) * cos(theta), sin(phi) * sin(theta), cos(phi), 0];

            dp = move * vec;
            snew = s + dp;
        end

        function snew = bulge(s0, pnear)
            % Grow the original sphere s0 without cutting nearby spheres
            % pnear.

            snew = s0;
            incMax = 1000;
            inc = 1;

            move = mean(pnear(:, 4)) / 20;

            while inc < incMax
                s = grow.nudge(snew, move);
                s(4) = max(grow.distance(s0, s), snew(4));

                if grow.cut(s, pnear)
                    % cut other particles
                    inc = inc + 1;
                else
                    [~, dr] = grow.near(s, pnear, 1);
                    s(4) = s(4) + dr; % bulge to touch the nearest sphere
                    snew = s;
                end

            end

        end

        function s = tune(s, psorted, pos)
            % Slightly adjust the position of sphere s according to
            % spheres around and test point pos.

            % check if possible to touch four particles
            for i = 4:size(psorted, 1)
                p4 = grow.stack(psorted(i, :), psorted(1:3, :));

                if ~grow.coplanar(p4)
                    snew = grow.touch(s, p4);

                    if grow.contain(snew, pos, psorted) && snew(4) > s(4)
                        s = snew;
                    end

                end

            end

            % check if possible to touch three particles
            p3 = grow.stack(pos, psorted(1:3, :));
            snew = grow.touch(s, p3);

            if grow.contain(snew, pos, psorted) && snew(4) > s(4)
                s = snew;
            end

        end

        function [s, ispore] = inscribe(pos, particles)
            % Find the largest sphere that contains the test point and
            % bounded by surrounding particles using a random nudge-bulge
            % method.

            % twenty nearest particles, should be sufficient
            ids = grow.near(pos, particles);
            pnear = particles(ids, :);

            % check if it is a throat
            [rnew, ~, posnew] = grow.toroidal(pos, pnear);

            if ~grow.cone(pos, pnear(1, :), pnear(2, :))
                rnew = rnew * 1000;
            end

            if ~grow.cut([posnew, rnew], pnear)
                s = [posnew, rnew];
                ispore = 0;
            else
                ispore = 1;
                % start nudge-bulge subroutine
                % initial position and radius
                s0 = [pos, 0];
                s = s0;

                % calculate three times and keep the largest solution

                for k = 1:3
                    sk = grow.bulge(s0, pnear);

                    % fine tune
                    % without these the curves in sc and fcc calculations are
                    % not straight

                    % update new near list
                    ids = grow.near(sk, pnear);
                    psorted = pnear(ids, :);
                    sk = grow.tune(sk, psorted, pos);
                    % compare the result
                    if sk(4) > s(4)
                        s = sk;
                    end

                end

            end

        end

    end

end
